<?php

class MY_Controller extends CI_Controller {

	function __construct() 
	{
		parent::__construct();
		//$this->set_session();
	}

	public function set_session()
	{
		if(strcasecmp($this->uri->segment(1), $this->router->fetch_class()) != 0)
		{
			// retrieve the session id from the routed uri
			session_name('PHPSESSID');
			$sid = $this->uri->segment(1);
			session_id($sid);
		}

		session_start();
	}
}  