<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Profile extends MY_Controller {

	/*
	 *
	 *   Display Name : None | Edit  *
     *   Profile Picture
     *   Date Registered : None | Edit  *
     *   Location : None | Edit
     *   Talent Gallery
     *   Censorship
     *   Age
	 */
	public function index()
	{
        $user_id = $this->session->userdata('admin_id');
        $this->load->model('profile_model', 'profile');

        $user_parent_details = $this->profile->get_details($user_id);
        $user_mobi_details = $this->profile->get_user_mobi_details($user_id);
        $user_mxit_details = $this->profile->get_user_mxit_details($user_id);

        $data['displayname'] = $user_parent_details['displayname'];
        $data['date_registered'] = $user_parent_details['date_registered'];
        $data['location'] = $user_mobi_details['area'];
        $data['censorship_id'] = $user_mxit_details['censorship_id'];
        $data['age_id'] = $user_parent_details['age_id'];

        //print_r($data);die();
        echo 'Parent table<br>';print_r($user_parent_details);echo '<br><br>';
        echo 'Mobi table<br>';print_r($user_mobi_details);echo '<br><br>';
        echo 'Mxit table<br>';print_r($user_mxit_details);die();
        display_view('Profile', 'profile', 'profile', $data);
	}
	
	
}