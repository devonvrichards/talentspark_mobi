<?php

class Stories extends MY_Controller
{
	
	
	function index($category_id)
	{	
		$this->load->model('stories_model', 'stories');

		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/site/stories/index/'.$category_id.'/';
		$config['total_rows'] = $this->stories->get_story_count($category_id);
		$config['per_page'] = 5; 
		$config['uri_segment'] = 5;
		$this->pagination->initialize($config);
		  
		$stories = $this->stories->get_stories(5, $this->uri->segment(5), $category_id);
		foreach($stories as $key=>$story){
			//$stories[$key]['star_rating'] = $this->stories->check_rating_binary($user_id, $story['id']);
			$stories[$key]['likes'] = $this->stories->get_rating_details($story['id'], 1);
		}
		$this->load->model('category_model', 'category');
		$category_name = $this->category->get_category_name($category_id);
        /*$this->load->view('stories/index',array('stories'=>$stories,
            'category_name'=>$category_name));*/
		display_view('All Traders', 'stories', 'index',array('stories'=>$stories,
            'category_name'=>$category_name) );
	}
	
	function view_story($story_id, $message=null)
	{
        $user_id = $this->session->userdata('admin_id');
		
		$this->load->model('stories_model', 'stories');
		$story_details = $this->stories->get_story($story_id);
		$total_pics = $this->stories->get_count_pics($story_id);
		$story_details['is_following'] = $this->stories->check_followering($user_id,$story_id);
		
		display_view('Service: '.$story_details['title'], 'stories', 'story', array(
					'story_details'=>$story_details,'total_pics'=>$total_pics,
						'message'=>$message));
	}	
	
	function view_users($category_id)
	{
        $user_id = $this->session->userdata('admin_id');
		
		$this->load->model('stories_model', 'stories');

		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/'.$user_id.'/stories/view_users/'.$category_id.'/';
		$config['total_rows'] = $this->stories->get_user_count($category_id);
		$config['per_page'] = 5; 
		$config['uri_segment'] = 5;
		$this->pagination->initialize($config);
		
		$stories = $this->stories->get_users(5, $this->uri->segment(5), $category_id);
		
		$this->load->model('category_model', 'category');
		$category_name = $this->category->get_category_name($category_id);

		display_view('All Traders', 'stories', 'view_talented', array('stories'=>$stories,
				'category_id'=>$category_id,
					'category_name'=>$category_name));
	}
	
	function author($story_id, $customer_id){
		$this->load->model('stories_model', 'stories');
		$author_details = $this->stories->get_author_details($customer_id);
		$author_details['stories'] = $this->stories->list_author_stories($customer_id);
		
		display_view('Author: '.$author_details['displayname'], 'stories', 'author', array(
								'story_id'=>$story_id,
								'author_details'=>$author_details));
	}
	public function rate_binary($category_id, $story_id){
		$this->load->model('stories_model', 'stories');
		
		$user_id = $_SESSION['user_id'];
		$star_rating = $this->stories->check_rating_binary($user_id, $story_id);
		if ($star_rating === FALSE)
			$value = $this->stories->insert_rating_binary($user_id, $story_id, 1);
		redirect(session_id().'/stories/index/'.$category_id);
	}
	
	//wunderkind sending a message to the event organizer
	public function send_message($story_id)
	{	
		display_view('Sending a message to a service', 'stories', 'send_message', array('story_id'=>$story_id));
	}
	public function insert_message($story_id)
	{	
		$input_details = $this->input->post();
		$text = $input_details['text'];
		if(!is_null($text)){
			$customer_id = $_SESSION['user_id'];
			$this->load->model('myinbox_model', 'myinbox');
			$this->myinbox->insert_message($customer_id, $story_id, $text);
		}
		redirect(session_id().'/stories/view_story/'.$story_id.'/1');
	}
	
	//event organizer sending a message to the wunderkind
	public function send_message_to_talented($customer_id,$category_id)
	{	
		display_view('Sending a message to a service', 'stories', 'send_message_to_talented', array('customer_id'=>$customer_id,'category_id'=>$category_id));
	}
	
	public function insert_message_to_talented($customer_id,$category_id)
	{	
		$input_details = $this->input->post();
		$text = $input_details['text'];
		if(!is_null($text)){
			$sending_customer_id = $_SESSION['user_id'];
			$this->load->model('stories_model', 'stories');
			$author_details = $this->stories->insert_message_to_talented($sending_customer_id,$customer_id,$text);
		}
		redirect(session_id().'/stories/view_talented_profile/'.$customer_id.'/'.$category_id.'/1');
	}
	
	
	public function talented_users($category_id)
	{
		$user_id = $_SESSION['user_id'];
		
		$this->load->model('stories_model', 'stories');
		$this->load->library('pagination');
		$config['base_url'] = base_url().'index.php/'.$user_id.'/stories/index/'.$category_id.'/';
		$config['total_rows'] = $this->stories->get_user_count($category_id);
		$config['per_page'] = 5; 
		$config['uri_segment'] = 5;
		$this->pagination->initialize($config);
		
		$this->load->model('category_model', 'category');
		$category_name = $this->category->get_category_name($category_id);
		
		$stories = $this->stories->get_talented_users(5, $this->uri->segment(5), $category_id);
		print_r($stories);
		foreach($stories as $key=>$story){
			$stories[$key]['star_rating'] = $this->stories->check_rating_binary($user_id, $story['id']);
			$stories[$key]['likes'] = $this->stories->get_rating_details($story['id'], 1);
		}
		display_view('All Traders', 'stories', 'talented', array('stories'=>$stories, 'category_name'=>$category_name));
	}
	
	public function view_talented_profile($customer_id,$category_id, $message=null)
	{
		
		$user_id = $_SESSION['user_id'];
		///////////////////////////////////////////////
		$this->load->model('stories_model', 'stories');
		$this->load->model('talent_model', 'talent');
		$this->load->model('profile_picture_model', 'profile_picture');
		
		$profile_details['profile_picture'] = $this->profile_picture->get_profile_picture_details($customer_id);
		$talent_details = $this->stories->get_talent_details($customer_id);
		$talents = $this->talent->get_customer_talents($customer_id);
	
		$total_pics = $this->stories->get_count_talented_pics($customer_id);
		$talent_details['is_following'] = $this->stories->check_talented_user_followering($user_id,$customer_id);
		
		display_view('Service: '.$story_details['title'], 'stories', 'talented_individuals_info', array(
					'story_details'=>$talent_details,'total_pics'=>$total_pics,'talents'=>$talents,
						'message'=>$message,'category_id'=>$category_id,'customer_id'=>$customer_id,
							'profile_details'=>$profile_details, 'message'=>$message));
	}
	
	function view_photos($customer_id,$category_id)
	{
		$user_id = $_SESSION['user_id'];
		$this->load->model('stories_model', 'stories');
		
		$photo_details = $this->stories->get_photos($customer_id);
		
		display_view('Stories', 'stories', 'view_talented_user_photos', array('photo_details'=>$photo_details,'customer_id'=>$customer_id,'category_id'=>$category_id));
	}



}
