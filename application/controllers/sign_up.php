<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Sign_up extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	
	}

    public function index()
    {
        $post = $this->input->post();

        $this->load->helper(array('form', 'url'));

        $this->load->library('form_validation');

        $this->form_validation->set_rules('username', 'Username', 'required');
        $this->form_validation->set_rules('password', 'Password', 'required');
        $this->form_validation->set_rules('passwordConfirm', 'Verify password', 'required');

        if ($this->form_validation->run() == FALSE)
        {

            display_view('Home', 'login', 're_sign_up', null);
        }
        else
        {

            $username = $post['username'];
            $txtConfirmPassword = $post['password'];
            $this->load->model('login_model', 'login');
            $decision = $this->login->validate_username($username);
            if($decision == NULL)
            {
                $this->login->create_parent_user();

                $parent_id = $this->db->insert_id();

                $this->login->create_mobi_user($username,$txtConfirmPassword, $parent_id);
                ("you successfully signed up");
                display_view('Home', null, 'login', null);
            }
            else
                die('user already exists');
        }
    }


    public function mxit_signup_view()
    {
        display_view('Mxit signup', null, 'mxit_verify', null);
    }

    public function mxit_signup()
    {
        $post = $this->input->post();


        $this->load->model('user_model', 'user');
        $parent_id = $this->user->get_user_parent_id($post['mxit_id']);
        //print_r($post);die();

        if(empty($parent_id))
        {
            display_view('Mxit signup', null, 'mxit_verify', array('error' => 'not found'));
        }
        else
        {
            $this->load->model('login_model', 'login');
            $this->login->create_mobi_user($post['username'],$post['password'],$parent_id['customer_parent_id']);
            redirect(base_url());
        }
    }
}

/* End of file sign_up.php */
/* Location: ./application/controllers/sign_up.php */
