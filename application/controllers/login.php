<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Login extends MY_Controller {

	public function __construct()
	{
		parent::__construct();

	}

	public function index($error = null)
	{
        //$this->load->view('login', array("error"=>$error));
       display_view('Home', null, 'login', array("error"=>$error));
	}

    public function validate_user()
    {
		$post_values = $this->input->post();
        $username = $post_values['username'];
        $password = $post_values['password'];

	    $this->load->model('login_model', 'login');
        $users = $this->login->validate_user($username,$password);

        if(empty($users))
        {
            $this->index("login_error");
        }
        else
        {
			$admin_id = $users[0]['customer_parent_id'];
			$random_hash = md5($admin_id);
			$ipaddress = $_SERVER["REMOTE_ADDR"];
			
			$data = array('admin_id' => $admin_id,'session_id' => $random_hash,'ip_address'=> $ipaddress,'is_logged_in' => true);   

			$this->session->set_userdata($data);
			redirect('site');
        }
    }


}

/* End of file login.php */
/* Location: ./application/controllers/login.php */
