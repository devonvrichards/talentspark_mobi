<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class User_perspective extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
	
	}

	public function index($customer_type_id)
	{
		//die($customer_type_id);
		display_view('Home', null, 'home', null);
	}

    public function change_perspective($customer_type_id)
    {
        $user_id = $this->session->userdata('admin_id');
        $this->load->model('profile_model', 'profile');
        $this->profile->change_perspective($user_id, $customer_type_id);
    }
}
