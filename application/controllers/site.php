<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Site extends MY_Controller {

	public function __construct()
	{
		parent::__construct();
		$this->_is_logged_in();
	}

	public function index()
	{
        $user_id = $this->session->userdata('admin_id');
        $data['user_id'] = $user_id;
        $this->load->model('user_model', 'user');

        $user_type = $this->user->get_user_type($user_id);
        $data['user_type'] = $user_type['customer_type_id'];
        //print_r();die();

        $this->load->model('stories_model', 'stories');
        $categories = $this->stories->get_categories();
        $data['categories'] = $categories;
        //print_r($categories);die();
        //$this->load->view('home', $data);
		display_view('Home', null, 'home', $data);
	}

    private function _is_logged_in()
	{
		$is_logged_in = $this->session->userdata('is_logged_in');
		
		if(!isset($is_logged_in) || $is_logged_in != true)
		{
			redirect('logout');
		}
	}
}

/* End of file site.php */
/* Location: ./application/controllers/site.php */
