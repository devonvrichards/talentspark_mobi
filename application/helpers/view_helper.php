<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('display_view'))
{
    function display_view($header, $view_folder, $main_content, $data = NULL)
    {
		$CI =& get_instance();

		$data['header'] = $header;

		if($view_folder == NULL)
		{
			$data['main_content'] = $main_content;
		}
		else
		{
			$data['main_content'] = $view_folder . '/' . $main_content;
		}

		$CI->load->view('includes/template', $data);
    }
	    function display_view_mobile($header, $view_folder, $main_content, $data = NULL)
    {
		$CI =& get_instance();

		$data['header'] = $header;

		if($view_folder == NULL)
		{
			$data['main_content'] = $main_content;
		}
		else
		{
			$data['main_content'] = $view_folder . '/' . $main_content;
		}

		$CI->load->view('includes/template_mobile', $data);
    }
	    
	    function browser_detection(){
	    	if (!empty($_SERVER['HTTP_CLIENT_IP']))   //check ip from share internet
	        {
	          $ip=$_SERVER['HTTP_CLIENT_IP'];
	        }
	        elseif (!empty($_SERVER['HTTP_X_FORWARDED_FOR']))   //to check ip is pass from proxy
	        {
	          $ip=$_SERVER['HTTP_X_FORWARDED_FOR'];
	        }
	        else
	        {
	          $ip=$_SERVER['REMOTE_ADDR'];
	        }
	        //$ua=$this->getBrowser();
	        //$yourbrowser= " browser: " . $ua['name'] . " " . $ua['version'];
	        //."".$yourbrowser;
	        return $ip;
	    }

	   /* private function getBrowser() 
	    { 
	        $u_agent = $_SERVER['HTTP_USER_AGENT']; 
	        $bname = 'Unknown';
	        $platform = 'Unknown';
	        $version= "";

	        //First get the platform?
	        if (preg_match('/linux/i', $u_agent)) {
	            $platform = 'linux';
	        }
	        elseif (preg_match('/macintosh|mac os x/i', $u_agent)) {
	            $platform = 'mac';
	        }
	        elseif (preg_match('/windows|win32/i', $u_agent)) {
	            $platform = 'windows';
	        }
	        
	        // Next get the name of the useragent yes seperately and for good reason
	        if(preg_match('/MSIE/i',$u_agent) && !preg_match('/Opera/i',$u_agent)) 
	        { 
	            $bname = 'Internet Explorer'; 
	            $ub = "MSIE"; 
	        } 
	        elseif(preg_match('/Firefox/i',$u_agent)) 
	        { 
	            $bname = 'Mozilla Firefox'; 
	            $ub = "Firefox"; 
	        } 
	        elseif(preg_match('/Chrome/i',$u_agent)) 
	        { 
	            $bname = 'Google Chrome'; 
	            $ub = "Chrome"; 
	        } 
	        elseif(preg_match('/Safari/i',$u_agent)) 
	        { 
	            $bname = 'Apple Safari'; 
	            $ub = "Safari"; 
	        } 
	        elseif(preg_match('/Opera/i',$u_agent)) 
	        { 
	            $bname = 'Opera'; 
	            $ub = "Opera"; 
	        } 
	        elseif(preg_match('/Netscape/i',$u_agent)) 
	        { 
	            $bname = 'Netscape'; 
	            $ub = "Netscape"; 
	        } 
	        
	        // finally get the correct version number
	        $known = array('Version', $ub, 'other');
	        $pattern = '#(?<browser>' . join('|', $known) .
	        ')[/ ]+(?<version>[0-9.|a-zA-Z.]*)#';
	        if (!preg_match_all($pattern, $u_agent, $matches)) {
	            // we have no matching number just continue
	        }
	        
	        // see how many we have
	        $i = count($matches['browser']);
	        if ($i != 1) {
	            //we will have two since we are not using 'other' argument yet
	            //see if version is before or after the name
	            if (strripos($u_agent,"Version") < strripos($u_agent,$ub)){
	                $version= $matches['version'][0];
	            }
	            else {
	                $version= $matches['version'][1];
	            }
	        }
	        else {
	            $version= $matches['version'][0];
	        }
	        
	        // check if we have a number
	        if ($version==null || $version=="") {$version="?";}
	        
	        return array(
	            'userAgent' => $u_agent,
	            'name'      => $bname,
	            'version'   => $version,
	            'platform'  => $platform,
	            'pattern'    => $pattern
	        );
	    } */
}

/* End of file view_helper.php */
/* Location: ./system/helpers/view_helper.php */