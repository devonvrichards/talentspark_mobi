<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');

if ( ! function_exists('get_auth'))
{

	function get_auth($scope, $client_id, $client_secret){
		if($scope ==1)
			$scope = 'profile/public';
		else if($scope == 2)
			$scope = 'message/send';
		
		/*cinemo*/
		$clientID = $client_id;
		$clientSecret= $client_secret;

		$request = curl_init();
		
		curl_setopt($request, CURLOPT_URL, "https://auth.mxit.com/token");
		curl_setopt($request, CURLOPT_HEADER,true);
		curl_setopt($request, CURLOPT_USERPWD,"$clientID:$clientSecret");
		curl_setopt($request, CURLOPT_POST, true);
		curl_setopt($request, CURLOPT_SSL_VERIFYPEER, false);
		curl_setopt($request, CURLOPT_POSTFIELDS, "grant_type=client_credentials&scope=".$scope);
		curl_setopt($request, CURLOPT_RETURNTRANSFER, true);
		
		$output = curl_exec($request);
		
		$details = explode(':',  $output);
		$access_token = $details[9];
		$details = explode(',',  $access_token);
		$token_with_qoutes = str_replace('"', "", $details[0]);
		return $token_with_qoutes;
	}
	
	function createHandle($msg, $realid, $token, $appname){
		$curlHandle = curl_init("http://api.mxit.com/message/send/");
		
		$msgObj = array(
				"Body" => $msg,
				"ContainsMarkup" => true,
				"From" => $appname,
				"To" => $realid,
				"Spool"=>true);
		
		//json encode the message
		$post = json_encode($msgObj);
		$defaultOptions = array (
				 CURLOPT_HTTPHEADER=>array('Content-type: application/json','Accept: application/json','Authorization: Bearer '.$token),
				 CURLOPT_POST=>true,
				 CURLOPT_POSTFIELDS=>$post,
				 CURLOPT_RETURNTRANSFER=>false);
		//send the message
		curl_setopt_array($curlHandle , $defaultOptions);

	    return $curlHandle;
	}
	function multiRequests($msg, $users, $token, $appname) {
		$mh = curl_multi_init();
		
	    $curlHandles = array();
	    $responses = array();

	    foreach($users as $id => $user) {
	        $curlHandles[$id] = createHandle($msg, $user['associative_id'], $token, $appname);
	        curl_multi_add_handle($mh, $curlHandles[$id]);
	    }

	    $running = null;
	    do {
	        curl_multi_exec($mh, $running);
	    } while($running > 0);

	    foreach($curlHandles as $id => $handle) {
	        $responses[$id] = curl_multi_getcontent($handle);
	        curl_multi_remove_handle($mh, $handle);
	    }
	    curl_multi_close($mh);

	    return $responses;
	}

}


?>
