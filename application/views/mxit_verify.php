<!DOCTYPE HTML>
<html>
<head>
    <title>Talentspark</title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="keywords" content="The free Portfoli Iphone web template, Andriod web template, Smartphone web template, free webdesigns for Nokia, Samsung, LG, SonyErricsson, Motorola web design" />
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">
    <script type="application/x-javascript"> addEventListener("load", function() { setTimeout(
            hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
    <link href="<?php echo base_url();?>css/login_style.css" rel="stylesheet" type="text/css"  media="all" />
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro" rel="stylesheet" type="text/css">
    <link href='http://fonts.googleapis.com/css?family=Josefin+Slab' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/default.css" />
    <link rel="stylesheet" type="text/css" href="<?php echo base_url();?>css/component.css" />
    </script>
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1/jquery.min.js"></script>
    <script type="text/javascript"><!--
        function checkPasswordMatch() {
            var password = $("#txtNewPassword").val();
            var confirmPassword = $("#txtConfirmPassword").val();

            if (password != confirmPassword)
            {
                $("#divCheckPasswordMatch").html("Passwords do not match!");
                $("#sub").attr('disabled','disabled');
            }
            else
            {
                $("#divCheckPasswordMatch").html("Passwords match.");
                $("#sub").removeAttr('disabled')
            }
        }
        //--></script>
</head>
<body>
<!---start-wrap--->
<!---start-header--->
<div class="header">
    <div class="wrap">
        <!---start-logo--->
        <div class="logo">
            <a href="#"><img alt="Talentspark Logo" src="<?php echo base_url();?>images/small Talent Spark Logo whitebg .png"> </a>
        </div>

        <div class="top-nav">
            <ul>
                <a href="<?php echo base_url();?>" class="scroll"><li  id="li" onclick="login()">Login</li></a>&nbsp;&nbsp;-&nbsp;&nbsp;
                <a href="<?php echo base_url();?>" class="scroll"><li id="reg" onclick="register()">Sign Up</li></a>&nbsp;&nbsp;-&nbsp;&nbsp;
                <a href="<?php echo base_url();?>index.php/site/sign_up/mxit_signup_view" class="scroll"><li id="mxit_reg">Sign up using Mxit ID</li></a>
            </ul>
        </div>
        <div class="clear"> </div>
        <!---End-top-nav--->
    </div>
</div>
<br><br><br>
<div class="contact" id="contact">
    <h3>Talentspark</h3>
    <p><center>Talent Spark, an opportunity for Talent and Scouts. This service allows for opportunity (scouts, event organizers etc.) and the talented to profile themselves, find opportunities and gain exposure through pictures, videos, audio and text.Create your spark!</center></p>
    <div id="mxit_signup">
        <form id="mxit_signup" action="<?php echo base_url().'index.php/site/sign_up/mxit_signup';?>" method="POST">
            <br><h3>Sign up using your mxit account</h3>
            <br>
            <h2>Mxit id<?php if(isset($error) && $error == 'not found'){?>&nbsp;&nbsp;-&nbsp;&nbsp;<font color="red" size="2px">Mxit id not found</font><?php }?></h2>
            <input name="mxit_id" type="text" class="textbox" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'mxit_id';}">

            <h2>Choose a Username<?php if(isset($error) && $error == 'no username'){?>&nbsp;&nbsp;-&nbsp;&nbsp;<font color="red" size="2px">No username entered</font><?php }?></h2>
            <input name="username" type="text" class="textbox" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Username';}">

            <h2>Password</h2>
            <div class="td">
                <input type="password" id="txtNewPassword" name="password"/>
            </div>
            <h2>Verify Password</h2>
            <div class="td">
                <input type="password" id="txtConfirmPassword" name="passwordConfirm" onkeyup="checkPasswordMatch();" />
            </div>
            <div class="registrationFormAlert" id="divCheckPasswordMatch">
            </div>


            <br><?php if(isset($error) && $error == 'passwords does not match'){?>&nbsp;&nbsp;-&nbsp;&nbsp;<font color="red" size="2px">Your passwords does not match</font><?php }?><br><br>
            </nr><input id="sub" type="submit" value="Sign Up">
        </form>
    </div>
</div>
</div>
</body>
</html>