<!-- Header -->
<div id="header">

    <!-- Inner -->
    <div class="inner">

    </div>

</div>

<!--Login Banner -->
<div id="banner" name="login">

       <div id="register" class="row">
        <article class="3u special">
            <br>
            <header>
                <h2><a href="#">Sign Up</a></h2>
            </header>
            <p>
			<?php echo validation_errors(); ?>
            <form action="<?php	echo base_url().'index.php/site/sign_up/';?>" method="POST">
                <label>Username</label><input type="text" value="" name="username" onfocus="this.value=''" id="Username"><br>
				<label>Password</label><input type="password" name="txtNewPassword" id="txtNewPassword"><br>
				<label>Verify password</label><input type="password" name="txtConfirmPassword" id="txtConfirmPassword"><br>
				<div class="registrationFormAlert" id="divCheckPasswordMatch"></div><br>
				
				
                <input type="submit" name="Submit" id="Submit" value="Sign Up">
            </form>
            </p>
        </article>
        <article class="8u special">
            <header>
                <h3>Hi. Welcome to <strong>Talentspark</strong>.</h3>
            </header>
            <p>
                <span class="byline">
					Talent Spark, an opportunity for Talent and Scouts. This service allows for opportunity (scouts, event organizers etc.) and the talented to profile themselves, find opportunities and gain exposure through pictures, videos, audio and text.Create your spark!
				</span>
            </p>
        </article>
    </div>
</div>
</body>
<div id="footer">
</div>