
<!-- Header -->
<div id="header">

    <!-- Inner -->
    <div class="inner">

    </div>

</div>

<!--Login Banner -->
<div id="banner" name="login">

    <div id="login" class="row">
        <article class="3u special">
            <br>
            <header>
                <h2><a href="#">Login</a></h2>
            </header>
            <p>
			<?php echo validation_errors(); ?>
            <form action="<?php	echo base_url().'index.php/site/login/';?>" method="POST">
                <label>Username</label><input type="text" value="" name="username" onfocus="this.value=''" id="Username"><br>
                <label>Password</label><input type="password" value="" name="password" onfocus="this.value=''" id="Password"><br>
                <input type="submit" name="Submit" id="Submit" value="Log in">
            </form>
            </p>
        </article>
        <article class="8u special">
            <header>
                <h3>Hi. Welcome to <strong>Talentspark</strong>.</h3>
            </header>
            <p>
                <span class="byline">
					Talent Spark, an opportunity for Talent and Scouts. This service allows for opportunity (scouts, event organizers etc.) and the talented to profile themselves, find opportunities and gain exposure through pictures, videos, audio and text.Create your spark!
				</span>
            </p>
        </article>
    </div>
</div>
</body>
<div id="footer">
</div>