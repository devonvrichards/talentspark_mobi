<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8" />
    <title>Talentspark_mobi</title>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0" />
    <link rel="stylesheet" media="all" href="<?php echo base_url();?>css/home_style.css" type="text/css">

</head>

<body>

<div class="wrap">
    <header>
        <div id='cssmenu'>
            <ul>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li class='has-sub'><a href='#'><img id="logo" src="<?php echo base_url();?>images/menu.png"> <b>Menu</b></a>
                    <ul>
                        <li class='has-sub'><a href='<?php echo base_url().'index.php/site/'; ?>'><span>Home</span></a></li>
                        <li class='has-sub'><a href='<?php echo base_url().'index.php/site/logout/index'; ?>'><span>Logout</span></a></li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="logo"><a href="<?php echo base_url().'index.php/site/'; ?>"><center><img id="logo" src="<?php echo base_url();?>images/small Talent Spark Logo whitebg .png"></center></a></div>

        <div class="clear"></div>


    </header>


    <div class="content">

        <article class="underline">
            Display Name : None | <a href='#'>Edit</a><br><br>
            
            Profile Picture<br><br>
            Date Registered : None | <a href='#'>Edit</a><br><br>
            Location : None | <a href='#'>Edit</a><br><br>

            Talent Gallery<br><br><br>


            Censorship<br><br>
            Age<br><br>
            <div class="clear"></div>
        </article>
        <article class="underline">

            <div class="clear"></div>
        </article>
    </div>
    <footer>
        <p>Talentspark 2013 &copy;</a></p>
    </footer>
</div>

<script src="<?php echo base_url();?>js/home_jquery.min.js"></script>
<script type="text/javascript">
    window.addEventListener("load",function() {
        // Set a timeout...
        setTimeout(function(){
            // Hide the address bar!
            window.scrollTo(0, 1);
        }, 0);
    });
    $('.search-box,.menu' ).hide();
    $('.options li:first-child').click(function(){
        $(this).toggleClass('active');
        $('.search-box').toggle();
        $('.menu').hide();
        $('.options li:last-child').removeClass('active');
    });
    $('.options li:last-child').click(function(){
        $(this).toggleClass('active');
        $('.menu').toggle();
        $('.search-box').hide();
        $('.options li:first-child').removeClass('active');
    });
    $('.content').click(function(){
        $('.search-box,.menu' ).hide();
        $('.options li:last-child, .options li:first-child').removeClass('active');
    });

    </body>
    </html>
