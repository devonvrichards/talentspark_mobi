<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Talentspark_mobi</title>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0"/>

    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>css/style.css" type="text/css">

    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/responsiveslides.css">
    <script src="<?php echo base_url(); ?>js/jquery.min.js"></script>
    <script src="<?php echo base_url(); ?>js/responsiveslides.min.js"></script>
</head>
