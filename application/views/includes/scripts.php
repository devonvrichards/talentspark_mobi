<?php
//set base url and user type in javascript for later use -- line 5, 153
echo '<script type="text/javascript">
          var user_type = '.$user_type.';
          var base_url = "'.base_url().'";
        </script>';

?>
<script type="text/javascript">
    $('#talent').slideUp();
    $('#scout').slideUp();
    function update()
    {
        if(user_type == 2)
        {
            $("#talent").slideDown();
            $("#scout").slideUp();

            $('#myonoffswitch').attr("checked", "checked")
        }
        else
        {
            $("#talent").slideUp();
            $("#scout").slideDown();

            $('#myonoffswitch').removeAttr("checked")
        }
    }
</script>
<script type="text/javascript">
    $(document).ready(update());

    $('#myonoffswitch').click(function () {
        var $this = $(this);
        var new_perspective = 0;
        // $this will contain a reference to the checkbox
        if ($this.is(':checked')) {
            // the checkbox was checked
            $('#talent').slideDown();
            $('#scout').slideUp();
            new_perspective = 2;
        } else {
            // the checkbox was unchecked
            $('#talent').slideUp();
            $('#scout').slideDown();
            new_perspective = 3;
        }
        //alert("Setting perspective to: " +new_perspective);
        $.post(base_url+'index.php/site/user_perspective/change_perspective/'+new_perspective);
    });


    window.addEventListener("load", function () {
        // Set a timeout...
        setTimeout(function () {
            // Hide the address bar!
            window.scrollTo(0, 1);
        }, 0);
    });
    $('.search-box,.menu').hide();
    $('.options li:first-child').click(function () {
        $(this).toggleClass('active');
        $('.search-box').toggle();
        $('.menu').hide();
        $('.options li:last-child').removeClass('active');
    });
    $('.options li:last-child').click(function () {
        $(this).toggleClass('active');
        $('.menu').toggle();
        $('.search-box').hide();
        $('.options li:first-child').removeClass('active');
    });
    $('.content').click(function () {
        $('.search-box,.menu').hide();
        $('.options li:last-child, .options li:first-child').removeClass('active');
    });
</script>