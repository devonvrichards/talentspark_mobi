<script type="text/javascript"><!--
    function checkPasswordMatch() {
        var password = $("#txtNewPassword").val();
        var confirmPassword = $("#txtConfirmPassword").val();

        if (password != confirmPassword)
            $("#divCheckPasswordMatch").html("Passwords do not match!");
        else
            $("#divCheckPasswordMatch").html("Passwords match.");
    }
    //--></script>
<script>
    $(function() {
        $( "#datepicker" ).datepicker();
    });
</script>
<script type="text/javascript">
    jQuery(document).ready(function($) {
        $(".scroll").click(function(event){
            event.preventDefault();
            $('html,body').animate({scrollTop:$(this.hash).offset().top},1200);
        });
    });
</script>
<script type="text/javascript">
    function login()
    {
        $("#login").slideDown();
        $("#register").slideUp();
        $("#cntc").slideUp();

        $("#li").addClass('active');
        $("#reg").removeClass('active');
        $("#cnt").removeClass('active');
    }

    function register()
    {
        $("#cntc").slideUp();
        $("#register").slideDown();
        $("#login").slideUp();

        $("#reg").addClass('active');
        $("#li").removeClass('active');
        $("#cnt").removeClass('active');
    }

    function contact()
    {
        $("#cntc").slideDown();
        $("#register").slideUp();
        $("#login").slideUp();

        $("#cnt").addClass('active');
        $("#li").removeClass('active');
        $("#reg").removeClass('active');
    }

    function init()
    {
        $("#cntc").slideUp();
        $("#register").slideUp();
        $("#login").slideUp();
    }
    $(document).ready(
        init()
    );
</script>