<!DOCTYPE HTML>
<!--
	Helios 1.0 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Talentspark</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600" rel="stylesheet" type="text/css"/>
    <!--[if lte IE 8]>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="<?php echo base_url();?>js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.dropotron.js"></script>
    <script src="<?php echo base_url();?>js/skel.min.js"></script>
    <script src="<?php echo base_url();?>js/skel-panels.min.js"></script>
    <script src="<?php echo base_url();?>js/init.js"></script>
    <noscript>
        <link rel="stylesheet" href="<?php echo base_url();?>css/skel-noscript.css"/>
        <link rel="stylesheet" href="<?php echo base_url();?>css/style.css"/>
        <link rel="stylesheet" href="<?php echo base_url();?>css/style-desktop.css"/>
        <link rel="stylesheet" href="<?php echo base_url();?>css/style-noscript.css"/>
        <link rel="stylesheet" href="<?php echo base_url();?>css/toggle_style.css"/>
    </noscript>

    <script type="text/javascript">

        $(document).ready(function(){
            $("h2").parent().next().hide();
        });
        $("h2").click(function (){
            $("h2").not(this).parent().next().hide();
            $(this).parent().next().show();
        });

        function initialise()
        {
            $('#register').slideUp();
            $('#login').slideUp();
            $('#banner').slideUp();
        }

        function login()
        {
            $('#register').slideUp();
            $('#login').slideDown();
            $('#banner').slideDown();
        }

        function register()
        {
            $('#banner').slideDown();
            $('#login').slideUp();
            $('#register').slideDown();
        }
    
	$(function() {
    $("#txtConfirmPassword").keyup(function() {
        var password = $("#txtNewPassword").val();
        $("#divCheckPasswordMatch").html(password == $(this).val() ? "<strong><font color='green'>Passwords match.<font></strong>" : "<strong><font color='red'>Passwords do not match!<font></strong>");
    });

});
	</script>

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="css/ie8.css"/><![endif]-->
</head>
<body class="homepage"  onload="initialise()">