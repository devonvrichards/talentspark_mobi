<body onload="init()">
<link rel="stylesheet" href="<?php echo base_url();?>css/landing_style.css"/>
<div class="wrap">
    <header>
        <div class="logo"><a  href="index.html"><img height="40px" width="100px" src="<?php echo base_url();?>images/small Talent Spark Logo whitebg .png" alt="Gallert Mobi - Free mobile website templates [Mobifreaks]"/></a></div>
        <div class="options">
            <ul>
                <li>Search</li>
                <li>Menu</li>
            </ul>
        </div>
        <div class="clear"></div>
        <div class="search-box">
            <form action="#">
                <input type="text" placeholder="Search gallery" />
                <input type="submit" value="Go" />
            </form>
            <div class="clear"></div>
        </div>
        <nav class="vertical menu">
            <ul>
                <li><a href="index.html">Home</a></li>
                <li><a href="single.html">Single page</a></li>
                <li><a href="basic_markup.html">Basic Markup Page</a></li>
                <li><a href="contact.html">Contact Page</a></li>
            </ul>
        </nav>
    </header>
    <link rel="stylesheet" href="<?php echo base_url();?>css/jquery.mobile-1.3.1.min.css">
    <script src="<?php echo base_url();?>js/jquery-1.9.1.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.mobile-1.3.1.min.js"></script>
    <link rel="stylesheet" href="<?php echo base_url();?>css/codiqa.ext.css">
    <script src="<?php echo base_url();?>js/codiqa.ext.js"></script>
    <div data-role="fieldcontain">
        <h3>&nbsp;&nbsp;Change user type here.</h3> <select name="toggleswitch1" id="toggleswitch1" data-theme="c" data-role="slider">
            <option value="3">
                Scout
            </option>
            <option value="2">
                Talented
            </option>
        </select>

    </div>

<div id="talent">

    <?php//stories index
        echo '<h4>Please choose an option to continue.</h4>';
        foreach($categories as $category)
        {
            echo '<a href="'.base_url().'index.php/site/stories/index/'.$category['id'].'">';
            echo '<h3>'.$category['name'].' ('.$category['total'].')</h3>';
            echo '<h5>'.$category['description'].'<h5><br><hr />';
            echo '<a href="">';
        }
    ?>
</div>
<div id="scout">
    <?php//stories view_users
    echo '<h4>Please choose an option to continue.</h4>';
    foreach($categories as $category)
    {
        echo '<a href="'.base_url().'index.php/site/stories/view_users/'.$category['id'].'">';
        echo '<h3>'.$category['name'].' ('.$category['total'].')</h3>';
        echo '<h5>'.$category['description'].'<h5><br><hr />';
        echo '<a href="">';
    }
    ?></div>
<script src="js/jquery.min.js"></script>
<script type="text/javascript">
    $("#scout").slideUp();
    $("#talent").slideUp();

    $( "#toggleswitch1" ).change(function() {
        //alert($( "#toggleswitch1" ).val());
        if($( "#toggleswitch1" ).val() == 2)
        {
            $("#scout").slideUp();
            $("#talent").slideDown();
        }
        else
        {
            $("#scout").slideDown();
            $("#talent").slideUp();
        }
    });
    window.addEventListener("load",function() {
        // Set a timeout...
        setTimeout(function(){
            // Hide the address bar!
            window.scrollTo(0, 1);
        }, 0);
    });
    $('.search-box,.menu' ).hide();
    $('.options li:first-child').click(function(){
        $(this).toggleClass('active');
        $('.search-box').toggle();
        $('.menu').hide();
        $('.options li:last-child').removeClass('active');
    });
    $('.options li:last-child').click(function(){
        $(this).toggleClass('active');
        $('.menu').toggle();
        $('.search-box').hide();
        $('.options li:first-child').removeClass('active');
    });
    $('.content').click(function(){
        $('.search-box,.menu' ).hide();
        $('.options li:last-child, .options li:first-child').removeClass('active');
    });
</script>
</body>
