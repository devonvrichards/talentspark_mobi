<?php $this->load->view('includes/home_extra.php');?>
<body>
<div class="wrap">
    <header>
        <div id='cssmenu'>
            <ul>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li class='has-sub'><a href='#'><img id="logo" src="<?php echo base_url(); ?>images/menu.png">
                        <b>Menu</b></a>
                    <ul>
                        <li class='has-sub'><a href='<?php echo base_url() . 'index.php/site/profile/index'; ?>'><span>Profile</span></a>
                        </li>
                        <li class='has-sub'><a href='<?php echo base_url() . 'index.php/site/logout/index'; ?>'><span>Logout</span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="logo"><a href="<?php echo base_url() . 'index.php/site/'; ?>">
                <center><img id="logo" src="<?php echo base_url(); ?>images/small Talent Spark Logo whitebg .png">
                </center>
            </a></div>
        <div class="clear"></div>
    </header>
    <div class="content">
        <article class="underline">
            <div class="onoffswitch">
                <input type="checkbox" name="onoffswitch" class="onoffswitch-checkbox" id="myonoffswitch">
                <label class="onoffswitch-label" for="myonoffswitch">
                    <div class="onoffswitch-inner"></div>
                    <div class="onoffswitch-switch"></div>
                </label>
            </div>
            <br>Change your user perspective here.

            <div class="top-nav">
                <ul style="list-style-type: none">
                    <div id="talent">
                        <h3>Event List</h3><br>
                        <?php
                        //print_r($categories);
                        foreach ($categories as $category) {
                            echo '<li><a href="' . base_url() . 'index.php/site/stories/index/' . $category['id'] . '">' . $category['name'] . ' (' . $category['total'] . ')</a></li>';
                            echo '<div class="clear"></div>';


                            /*echo '<a href="' . base_url() . 'index.php/site/stories/index/' . $category['id'] . '">';
                            echo '<h4>' . $category['name'] . ' (' . $category['total'] . ')</h4>';*/
                        }
                        ?>
                    </div>
                </ul>
            </div>

            <div class="top-nav">
                <ul style="list-style-type: none">
                    <div id="scout">
                        <h3>Talented People</h3><br>
                        <?php
                        //print_r($categories);
                        foreach ($categories as $category) {
                            echo '<li><a href="' . base_url() . 'index.php/site/stories/view_users/' . $category['id'] . '">' . $category['name'] . ' (' . $category['talent_total'] . ')</a></li>';
                            echo '<div class="clear"></div>';


                            /*echo '<a href="' . base_url() . 'index.php/site/stories/index/' . $category['id'] . '">';
                            echo '<h4>' . $category['name'] . ' (' . $category['total'] . ')</h4>';*/
                        }
                        ?>
                    </div>
                </ul>
            </div>

            <div class="clear"></div>
        </article>
        <article class="underline">

            <div class="clear"></div>
        </article>
    </div>
</div>
<?php $this->load->view('includes/scripts.php');?>
</body>
</html>
