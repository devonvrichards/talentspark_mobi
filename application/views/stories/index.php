<!DOCTYPE html>

<html lang="en">
<head>
    <meta charset="utf-8"/>
    <title>Talentspark_mobi</title>
    <meta name="viewport" content="width=device-width; initial-scale=1.0; maximum-scale=1.0"/>
    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>css/home_style.css" type="text/css">
    <link rel="stylesheet" media="all" href="<?php echo base_url(); ?>css/style.css" type="text/css">

    <link href='http://fonts.googleapis.com/css?family=Raleway' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo base_url(); ?>css/responsiveslides.css">
    <script src="<?php echo base_url(); ?>js/responsiveslides.min.js"></script>

</head>

<body>

<div class="wrap">
    <header>
        <div id='cssmenu'>
            <ul>
                <li>&nbsp;</li>
                <li>&nbsp;</li>
                <li class='has-sub'><a href='#'><img id="logo" src="<?php echo base_url(); ?>images/menu.png">
                        <b>Menu</b></a>
                    <ul>
                        <li class='has-sub'><a href="<?php echo base_url(); ?>"><span>Home</span></a>
                        </li>
                        <li class='has-sub'><a href='<?php echo base_url() . 'index.php/site/profile/index'; ?>'><span>Profile</span></a>
                        </li>
                        <li class='has-sub'><a href='<?php echo base_url() . 'index.php/site/logout/index'; ?>'><span>Logout</span></a>
                        </li>
                    </ul>
                </li>
            </ul>
        </div>

        <div class="logo"><a href="<?php echo base_url() . 'index.php/site/'; ?>">
                <center><img id="logo" src="<?php echo base_url(); ?>images/small Talent Spark Logo whitebg .png">
                </center>
            </a></div>
        <div class="clear"></div>
    </header>
    <div class="list_content">
        <div class="content-grids">
            <!-- STORY CONTENT Array ( [id] => "" [customer_id] => "" [category_id] => "" [description] => "" [timestamp] => "" [author_name] => "" [status] => "") -->
            <?php foreach ($stories as $story) { ?>
                <div class="grid">
                    <h4 class="first"><?php echo $story['title']; ?></h4>
                    <p>
                        <?php echo $story['extract']; ?>
                    </p>
                    <a style="float: inherit" href="<?php echo base_url() . 'index.php/site/stories/view_talented_profile/'.$story['customer_id'].'/'.$story['category_id']; ?>">Read More</a>
                </div>
            <?php } ?>
            <div class="clear"></div>
        </div>

        <div class="clear"></div>
        <span style="float: right"><?php echo $this->pagination->create_links();?></span>
    </div>
    <footer>
        <span style="text-align: center"><p>Talentspark 2013 &copy;</a></p></span>
    </footer>
</div>

<script src="<?php echo base_url(); ?>js/home_jquery.min.js"></script>
<script>

    window.addEventListener("load", function () {
        // Set a timeout...
        setTimeout(function () {
            // Hide the address bar!
            window.scrollTo(0, 1);
        }, 0);
    });
    $('.search-box,.menu').hide();
    $('.options li:first-child').click(function () {
        $(this).toggleClass('active');
        $('.search-box').toggle();
        $('.menu').hide();
        $('.options li:last-child').removeClass('active');
    });
    $('.options li:last-child').click(function () {
        $(this).toggleClass('active');
        $('.menu').toggle();
        $('.search-box').hide();
        $('.options li:first-child').removeClass('active');
    });
    $('.content').click(function () {
        $('.search-box,.menu').hide();
        $('.options li:last-child, .options li:first-child').removeClass('active');
    });
</script>
</body>
</html>