
<?php $this->load->view('includes/login_extra.php');?>
<body>
<!---start-wrap--->
<!---start-header--->
<div class="header">
    <div class="wrap">
        <!---start-logo--->
        <div class="logo">
            <a href="#"><img alt="Talentspark Logo" src="<?php echo base_url();?>images/small Talent Spark Logo whitebg .png"> </a>
        </div>

        <div class="top-nav">
            <ul>
                <a href="#login_form" class="scroll"><li  id="li" onclick="login()">Login</li></a>&nbsp;&nbsp;-&nbsp;&nbsp;
                <a href="#register_form" class="scroll"><li id="reg" onclick="register()">Sign Up</li></a>&nbsp;&nbsp;-&nbsp;&nbsp;
                <a href="<?php echo base_url();?>index.php/site/sign_up/mxit_signup_view" class="scroll"><li id="mxit_reg">Sign up using Mxit ID</li></a>
            </ul>
        </div>
        <div class="clear"> </div>
        <!---End-top-nav--->
    </div>
</div>
<br><br><br>
<div class="contact" id="contact">
    <h3>Talentspark</h3>
    <p><center>Talent Spark, an opportunity for Talent and Scouts. This service allows for opportunity (scouts, event organizers etc.) and the talented to profile themselves, find opportunities and gain exposure through pictures, videos, audio and text.Create your spark!</center></p>
    <div id="login">
        <form id="login_form" action="<?php echo base_url().'index.php/site/login/validate_user';?>" method="POST">
            <h3>Log in</h3>
            <br>
            <h2>Username</h2>
            <input name="username" type="text" class="textbox" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Username';}">
            <h2>Password</h2>
            <input name="password" type="password" class="password" value="" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}">
            <br>
            </nr><input type="submit" value="Log In">
        </form>
    </div>

    <div id="register">
        <form id="register_form" action="<?php echo base_url().'index.php/site/sign_up/';?>" method="POST">
            <h3>Sign Up</h3>
            <br>

            <h2>Username</h2>
            <input name="username" type="text" class="textbox" value="" onfocus="this.value = '';" >

            <h2>Password</h2>
            <div class="td">
                <input type="password" id="txtNewPassword" name="password"/>
            </div>
            <h2>Verify Password</h2>
            <div class="td">
                <input type="password" id="txtConfirmPassword" name="passwordConfirm" onkeyup="checkPasswordMatch();" />
            </div>
            <div class="registrationFormAlert" id="divCheckPasswordMatch">
            </div>




            <br>
            </nr><input type="submit" value="Sign up">
        </form>
    </div>
</div>

</div>
</body>
</html>
<?php $this->load->view('includes/login_script.php');?>
