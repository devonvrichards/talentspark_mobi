<?php
//print_r($error.'-View');die();
echo '<script type="text/javascript">var error = '.$error.';});</script>';
?>
<!DOCTYPE HTML>
<!--
	Helios 1.0 by HTML5 UP
	html5up.net | @n33co
	Free for personal and commercial use under the CCA 3.0 license (html5up.net/license)
-->
<html xmlns="http://www.w3.org/1999/html">
<head>
    <title>Talentspark</title>
    <meta http-equiv="content-type" content="text/html; charset=utf-8"/>
    <meta name="description" content=""/>
    <meta name="keywords" content=""/>
    <link href="http://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600" rel="stylesheet" type="text/css"/>
    <!--[if lte IE 8]>
    <script src="js/html5shiv.js"></script><![endif]-->
    <script src="<?php echo base_url();?>js/jquery.min.js"></script>
    <script src="<?php echo base_url();?>js/jquery.dropotron.js"></script>
    <script src="<?php echo base_url();?>js/skel.min.js"></script>
    <script src="<?php echo base_url();?>js/skel-panels.min.js"></script>
    <script src="<?php echo base_url();?>js/init.js"></script>
    <script src="<?php echo base_url();?>js/jquery.js"></script>
    <script style="text/css">
        <link rel="stylesheet" href="<?php echo base_url();?>css/skel-noscript.css"/>
            <link rel="stylesheet" href="<?php echo base_url();?>css/style.css"/>
            <link rel="stylesheet" href="<?php echo base_url();?>css/style-desktop.css"/>
            <link rel="stylesheet" href="<?php echo base_url();?>css/style-noscript.css"/>
            <link rel="stylesheet" href="<?php echo base_url();?>css/toggle_style.css"/>
    </script>
    <script type="text/javascript">
        var error = "<?php echo $error;?>";
        $(document).ready(function(){
            $("h2").parent().next().hide();
        });
        $("h2").click(function (){
            $("h2").not(this).parent().next().hide();
            $(this).parent().next().show();
        });

        function initialise()
        {
            if(error == "login_error")
            {
                $('#register').slideUp();
                $('#login').slideDown();
                $('#banner').slideDown();
                $('#login_error').slideDown();
                $('#header').slideUp();
            }
            else
            {
                $('#register').slideUp();
                $('#login').slideUp();
                $('#banner').slideUp();
                $('#login_error').slideUp();
            }
        }
        function login()
        {
            $('#register').slideUp();
            $('#login').slideDown();
            $('#banner').slideDown();
        }

        function register()
        {
            $('#banner').slideDown();
            $('#login').slideUp();
            $('#register').slideDown();
        }

        $(function() {
            $("#txtConfirmPassword").keyup(function() {
                var password = $("#txtNewPassword").val();
                $("#divCheckPasswordMatch").html(password == $(this).val() ? "<strong><font color='green'>Passwords match.<font></strong>" : "<strong><font color='red'>Passwords do not match!<font></strong>");
            });

        });
    </script>

    <!--[if lte IE 8]>
    <link rel="stylesheet" href="css/ie8.css"/><![endif]-->
</head>
<body class="homepage"  onload="initialise()">
<div id="header">

    <!-- Inner -->
    <div class="inner">
        <header>
            <img id="logo" src="<?php echo base_url();?>images/small Talent Spark Logo whitebg .png">
            <hr/>
            <span class="byline">Where talent and opportunity meet.</span>
        </header>
        <footer>
            <a onclick="login()" href="#banner" class="button circled scrolly">Login</a>
            <a onclick="register()" href="#banner" class="button circled scrolly">Sign Up</a>
        </footer>
    </div>

    <!-- Nav -->
</div>

<!--Login Banner -->
<div id="banner" name="login">

    <div id="login" class="row">
        <article class="3u special">
            <br>
            <header>
                <h2><a href="#">Login</a></h2>
            </header>
            <p>
                <?php echo validation_errors(); ?>
            <form id="login_form"  action="<?php echo base_url().'index.php/site/login/validate_user';?>" method="POST">
                <label>Username</label><input  type="text" value="brandon" name="username" onfocus="this.value=''" id="Username"><br>
                <label>Password</label><input type="password" value="brandon" name="password" onfocus="this.value=''" id="Password"><br>
                <div id="login_error"><label style="color: #ff0300">The username or password you entered is incorrect.</label></div>
                <input type="submit" name="Submit" id="Submit" value="Log in">
            </form>
            </p>
        </article>
        <article class="8u special">
            <header>
                <h3>Hi. Welcome to <strong>Talentspark</strong>.</h3>
            </header>
            <p>
                <span class="byline">
					Talent Spark, an opportunity for Talent and Scouts. This service allows for opportunity (scouts, event organizers etc.) and the talented to profile themselves, find opportunities and gain exposure through pictures, videos, audio and text.Create your spark!
				</span>
            </p>
        </article>
    </div>
    <div id="register" class="row">
        <article class="3u special">
            <br>
            <header>
                <h2><a href="#">Sign Up</a></h2>
            </header>
            <p>
                <?php echo validation_errors(); ?>
            <form action="<?php	echo base_url().'index.php/site/sign_up/';?>" method="POST">
                <label>Username</label><input type="text" value="" name="username" onfocus="this.value=''" id="Username_reg"><br>
                <label>Password</label><input type="password" name="txtNewPassword" id="txtNewPassword"><br>
                <label>Verify password</label><input type="password" name="txtConfirmPassword" id="txtConfirmPassword"><br>
                <div class="registrationFormAlert" id="divCheckPasswordMatch"></div><br>


                <input type="submit" name="Submit" id="Submit" value="Sign Up">
            </form>
            </p>
        </article>
        <article class="8u special">
            <header>
                <h3>Hi. Welcome to <strong>Talentspark</strong>.</h3>
            </header>
            <p>
                <span class="byline">
					Talent Spark, an opportunity for Talent and Scouts. This service allows for opportunity (scouts, event organizers etc.) and the talented to profile themselves, find opportunities and gain exposure through pictures, videos, audio and text.Create your spark!
				</span>
            </p>
        </article>
    </div>
</div>
</body>
<div id="footer">
</div>

<script type="text/javascript">

    function test(){alert('returning false');return false;}

    function verify()
    {

        if($("#Username").val().length == 0 || $("#Password").val().length == 0 )
        {
            $("#login_error").slideDown();
            $("#Username").val("");
            $("#Password").val("");
            $("#Username").focus();
            return false;
        }

        var ret = false;
        var posted= $.ajax({
            url: 'index.php/site/login/validate_user/'+$("#Username").val()+'/'+$("#Password").val(),
            data: {username: $("#Username").val(), password:$("#Password").val()}//values posted
        });
        //alert(posted.toSource());return false;
        posted.success(function(data)//returned data
        {
            //alert("Data: "+data);return false;
            if(data == "0")
            {
                $("#login_error").slideDown();
                $("#Password").val("");
                $("#Password").focus();
            }
            else
            {
                window.location.replace('http://localhost/talentspark_mobi/index.php/site/login/index/'+data);
            }
        });
        return false;
    }
</script>
</section>


</body>

</html>