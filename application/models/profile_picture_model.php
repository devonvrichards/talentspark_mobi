<?php

class Profile_picture_model extends CI_Model {


	function get_profile_picture_details($user_id){
    	$q = $this
  			  ->db
  			  ->select('*')
  			  ->from('customer_picture')
  			  ->where('customer_id', $user_id)
  			  ->where('status', 1)
  			  ->get()
  			  ->row_array();
  		return $q;
    }
    function disable_last_profile_picture($customer_id){
		$data = array('status' => 0);

		$this->db->where('customer_id', $customer_id);
		$this->db->update('customer_picture', $data); 
	}
	function insert_profile_picture($user_id, $file_name){
		
		$new_profile_picture = array('customer_id'=> $user_id, 
								'file_name'=>$file_name,
									'status'=>1);
					
		$insert = $this->db->insert('customer_picture', $new_profile_picture);
		return $insert;
	}
	
}