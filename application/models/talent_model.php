<?php

class Talent_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

    function get_customer_talents($user_id){
        $q = $this
            ->db
            ->select('*, (select name from category where id = customer_talents.category_id) as category_name')
            ->from('customer_talents')
            ->where('customer_id', $user_id)
            ->get()
            ->result_array();
        return $q;
    }
    function add_talent($customer_id, $description, $category_id){
        $data = array(
            'customer_id'=> $customer_id,
            'description' => $description,
            'category_id'=> $category_id);

        $this->db->insert('customer_talents', $data);
        return $this->db->insert_id();
    }
    function remove_talent($customer_talents_id){
        $this->db->delete('customer_talents', array('id' => $customer_talents_id));
    }

}