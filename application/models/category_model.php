<?php

class Category_model extends CI_Model {

	function __construct()
	    {
		parent::__construct();
	    }
	public function get_category_name($category_id){
		$q = $this
				  ->db
				  ->select('name')
				  ->from('category')
				  ->where('id', $category_id)
				  ->get()
				  ->row_array();
			return $q['name'];	
	}

	public function get_categories(){
		$q = $this
            ->db
			  ->select('*, (select count(id) from story where story.category_id = category.id and story.enabled=1) as category_count')
			  ->from('category')
			  ->where('enabled',1)
			  ->order_by('id', 'desc')
			  ->get()
			  ->result_array();
		return $q;
		
	}
	public function get_titles($category_id){
		$q = $this
			  ->db
			  ->select('id, title')
			  ->from('story')
			  ->where('cat_id', $category_id)
			  ->order_by('date_created', 'desc')
			  ->limit(2)
			  ->get()
			  ->result_array();
		return $q;	
	}
	//view type 1 - most recent
	public function get_all_titles($category_id, $limit, $offset){
		$q = $this
			  ->db
			  ->select('*,
(select name from publisher where id=story.publisher_id) as publisher_name')
			  ->from('story')
			  ->where('cat_id', $category_id)
			  ->where('enabled',1)
			  ->where('sub_story_type != 2')
			  ->order_by('date_created', 'desc')
			  ->limit($limit, $offset)
			  ->get()
			  ->result_array();
		return $q;
	}
	public function get_title_count($category_id){
		$q = $this
			  ->db
			  ->select('count(*) as count')
			  ->from('story')
			  ->where('cat_id', $category_id)
			  ->where('enabled',1)
			  ->get()
			  ->row_array();
		return $q['count'];
	}
	public function get_star_rating($video_id){
		$q = $this
			  ->db
			  ->select('avg(rating_id) as avg')
			  ->from('rating_story')
			  ->where('story_id', $video_id)
			  ->get()
			  ->row_array();
		return $q['avg'];	
	}	

	/*
	*
	*
	* new filtering totals
	*
	*/
	public function get_story_info($story_id){
		$q = $this
			  ->db
			  ->select('*,(select name from publisher where id=story.publisher_id) as publisher_name')
			  ->from('story')
			  ->where('id', $story_id)
			  ->where('enabled',1)
			  ->get()
			  ->row_array();
		return $q;
	}
	
	
	/*
	*
	*
	*Comment filter
	*
	*
	*/
	public function get_latest_comment($limit){
		$q = $this
				  ->db
				  ->select('story_id')
				  ->from('comment_story')
				  ->where('day(timestamp) = day(curdate())')
				  ->where('week(timestamp) = week(curdate())')
				  ->where('month(timestamp) = month(curdate())')
				  ->where('year(timestamp) = year(curdate())')
				  ->group_by('story_id')
				  ->order_by('timestamp', 'desc')
				  ->order_by('story_id', 'asc')
				  ->limit($limit)
				  ->get()
				  ->result_array();
			return $q;
	}
	
	public function most_commented_this_week($limit){
		$q = $this
				  ->db
				  ->select('story_id')
				  ->from('comment_story')
				  ->where('week(timestamp) = week(curdate())')
				  ->where('month(timestamp) = month(curdate())')
				  ->where('year(timestamp) = year(curdate())')
				  ->group_by('story_id')
				  ->order_by('timestamp', 'desc')
				  ->order_by('story_id', 'asc')
				  ->limit($limit)
				  ->get()
				  ->result_array();
			return $q;
	}
	
	public function most_commented_ever($limit){
		$q = $this
				  ->db
				  ->select('*, 
						  (select name from publisher where id=story.publisher_id) as publisher_name, 
						  (select count(id) from comment_story where story_id=story.id) as comment_total')
				  ->from('story')
				  ->where('story.enabled', 1)
				  ->where('year(date_created) = year(curdate())')
				  ->group_by('id')
				  ->order_by('comment_total', 'desc')
				  ->order_by('date_created', 'desc')
				  ->limit($limit)
				  ->get()
				  ->result_array();
			return $q;
	}
	
	/*
	*
	*
	*liked filter
	*
	*
	*/
	
	
	public function get_latest_rated($limit){
		$q = $this
				  ->db
				  ->select('story_id')
				  ->from('rating_comment_story_binary')
				  ->where('day(timestamp) = day(curdate())')
				  ->where('week(timestamp) = week(curdate())')
				  ->where('month(timestamp) = month(curdate())')
				  ->where('year(timestamp) = year(curdate())')
				  ->group_by('story_id')
				  ->order_by('timestamp', 'desc')
				  ->order_by('story_id', 'asc')
				  ->limit($limit)
				  ->get()
				  ->result_array();
			return $q;
	}

	public function most_liked_this_week($limit){
		$q = $this
				  ->db
				  ->select('story_id, count(id) as total')
				  ->from('rating_comment_story_binary')
				  ->where('week(timestamp) = week(curdate())')
				  ->where('month(timestamp) = month(curdate())')
				  ->where('year(timestamp) = year(curdate())')
				  ->group_by('story_id')
				  ->order_by('timestamp', 'desc')
				  ->order_by('story_id', 'asc')
				  ->limit($limit)
				  ->get()
				  ->result_array();
			return $q;
	}
	
	public function most_liked_ever($limit){
		$q = $this
				  ->db
				  ->select('*, 
						  (select name from publisher where id=story.publisher_id) as publisher_name, 
						  (select count(id) from rating_comment_story_binary where rating_comment_story_binary.story_id = story.id and rating_comment_story_binary.rating_comment_id = 1) as liked_total')
				  ->from('story')
				  ->where('story.enabled',1)
				  ->where('year(story.date_created) = year(curdate())')
				  ->group_by('story.id')
				  ->order_by('liked_total', 'desc')
				  ->order_by('story.date_created', 'desc')
				  ->limit($limit)
				  ->get()
				  ->result_array();
			return $q;
	}
	/*
	*
	*
	*Latest published content
	*
	*
	*/
	
	public function most_recent($limit){
		$q = $this
				  ->db
				  ->select('*, (select name from publisher where id=story.publisher_id) as publisher_name')
				  ->from('story')
				  ->where('enabled',1)
				  ->order_by('date_created', 'desc')
				  ->limit($limit)
				  ->get()
				  ->result_array();
			return $q;
	}
}

