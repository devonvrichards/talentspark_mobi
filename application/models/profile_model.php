<?php

class Profile_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }
    
    function update_last_timestamp($user_id){
		$data = array('date_last_activate'=>date('Y-m-d H:i:s', time()+7200));
		
		$this->db->where('id', $user_id);
		$this->db->update('customer', $data); 
	}
    
    function change_perspective($customer_id, $customer_type_id){
		$data = array('customer_type_id'=>$customer_type_id);
		
		$this->db->where('id', $customer_id);
		$this->db->update('customer_mobi_extra', $data);
    }

	/*function get_details($user_id)
	{ 
		$q = $this
			  ->db
			  ->select('*, (select text from censorship where id = customer.censorship_id) as censorship_name,
			  				(select count(id) from personlised_events where customer_id = customer.id) as total_services,(select count(id) from personlised_customers where customer_id = customer.id) as total_following,
			  				(select text from age where id = customer.age_id) as age_range,
			  				(select name from customer_type where id = customer.customer_type_id) as customer_type')
			  ->from('customer')
			  ->where('id', $user_id)
			  ->get()
			  ->row_array();
		return $q;
	}*/

    function get_details($user_id)
    {
        $q = $this
            ->db
            ->select('*')
            ->from('customer_parent')
            ->where('id', $user_id)
            ->get()
            ->row_array();
        return $q;
    }

    function get_user_mobi_details($user_id)
    {
        $q = $this
            ->db
            ->select('*')
            ->from('customer_mobi_extra')
            ->where('customer_parent_id', $user_id)
            ->get()
            ->row_array();
        return $q;
    }

    function get_user_mxit_details($user_id)
    {
        $q = $this
            ->db
            ->select('*')
            ->from('customer_mxit_extra')
            ->where('customer_parent_id', $user_id)
            ->get()
            ->row_array();
        return $q;
    }

	function get_suburb_id($user_id)
	{
		$q = $this
		->db
		->select('*')
				  				->from('customer')
				  				->where('id', $user_id)
				  				->get()
				  				->row_array();
		return $q;
	}
	function get_customer_information($user_id)
	{ 
		$q = $this
			  ->db
			  ->select('*')
			  ->from('customer_information')
			  ->where('customer_id', $user_id)
			  ->get()
			  ->row_array();
		return $q;
	}
	function get_customer_types(){
		$q = $this
				  ->db
				  ->select('*')
				  ->from('customer_type')
				  ->get()
				  ->result_array();
			return $q;
	}
	
	function get_talents($user_id)
	{ 
		$q = $this
			  ->db
			  ->select('*,(select name from category where id = user_talents.category_id) as category_name')
			  ->from('customer_talents')
			  ->where('customer_id', $user_id)
			  ->get()
			  ->result_array();
		return $q;
	}
	
	function get_category()
	{
		$q = $this
			  ->db
			  ->select('*')
			  ->from('category')
			  ->get()
			  ->result_array();
		return $q;
	}
	
	function get_age()
	{
		$q = $this
			  ->db
			  ->select('*')
			  ->from('age')
			  ->get()
			  ->result_array();
		return $q;
	}
	
	function update_age_add($customer_id,$age_id){
		$data = array(
		       'age_id' => $age_id
		    );
		$this->db->where('id', $customer_id);
		$this->db->update('customer', $data); 
		
	}
	
	function update_suburb_id($user_id,$suburb_id){
		$data = array(
				'suburb_id' => $suburb_id
		);
		$this->db->where('associative_id', $user_id);
		$this->db->update('customer', $data);
	
	}

	function get_age_range($age_id){
				
			$q = $this
			  ->db
			  ->select('text')
			  ->from('age')
			  ->where('id',$age_id)
			  ->get()
			  ->row_array();
		return $q['text'];
	}

	function get_censorship($id)
	{
		$q = $this
			  ->db
			  ->select('text')
			  ->from('censorship')
			  ->where('id', $id)
			  ->get()
			  ->row_array();
		return $q['text'];
	}
	function get_all_censorships()
	{
		$q = $this
			  ->db
			  ->select('id, text')
			  ->from('censorship')
			  ->get()
			  ->result_array();
		return $q;
	}
	function update_censorship($user_id, $censorship_id)
	{
		$data = array(
		       'censorship_id' => $censorship_id
		    );

		$this->db->where('id', $user_id);
		$this->db->update('customer', $data); 
	}
	
	function update_displayname($user_id, $displayname_text)
	{
		$data = array('displayname' => $displayname_text);

		$this->db->where('id', $user_id);
		$this->db->update('customer', $data); 
	}
	
	function update_status_message($customer_information_id, $status_message)
	{
		$data = array('status_message' => $status_message);

		$this->db->where('id', $customer_information_id);
		$this->db->update('customer_information', $data); 
	}
	
	function update_background($customer_information_id, $background)
	{
		$data = array('background' => $background);

		$this->db->where('id', $customer_information_id);
		$this->db->update('customer_information', $data); 
	}
	
	function activate_customer_information($user_id)
	{
		$customer_info = array('customer_id'=>$user_id);
					
		$insert = $this->db->insert('customer_information', $customer_info);
	}

    
}

