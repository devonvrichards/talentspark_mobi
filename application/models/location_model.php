<?php

class Location_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

	function get_countries(){
		$q = $this
			  ->db
			  ->select('*')
			  ->from('country')
			  ->get()
			  ->result_array();	
		return $q;
	}
	
	function get_provinces($country_id){
		$q = $this
			  ->db
			  ->select('*')
			  ->from('province')
			  ->where('country_id', $country_id)
			  ->get()
			  ->result_array();	
		return $q;
	}
	/*
	 select *,(select count(*) from region where province = province.id and (select count(*) from suburb where region.id = suburb.region) ) as total
from province
where country_id = 1
having total > 1
	 */
	function get_regions($province_id){
		$q = $this
			  ->db
			  ->select('*')
			  ->from('region')
			  ->where('province_id', $province_id)
			  ->get()
			  ->result_array();	
		return $q;
		
	}
	
	function get_region_name($region_id){
	
		$q = $this
			  ->db
			  ->select('name')
			  ->from('region')
			  ->where('id', $region_id)
			  ->get()
			  ->row_array();	
		return $q['name'];
	
	}
	
	function get_suburbs($region_id){
	
		$q = $this
			  ->db
			  ->select('*')
			  ->from('suburb')
			  ->where('region_id', $region_id)
			  ->get()
			  ->result_array();	
		return $q;
	
	}
	
	function get_surbub_name($suburb_id){
	
		$q = $this
			  ->db
			  ->select('name')
			  ->from('suburb')
			  ->where('id', $suburb_id)
			  ->get()
			  ->row_array();	
		return $q['name'];
	
	}
	
	function get_locations($suburb_id){
		
		$q = $this
			  ->db
			  ->select('*')
			  ->from('location')
			  ->where('location_suburb_id', $suburb_id)
			  ->get()
			  ->result_array();	
		return $q;
	
	}
	
	function get_suburb_name($suburb_id){
		
	$q = $this
			  ->db
			  ->select('*')
			  ->from('suburb')
			  ->where('id', $suburb_id)
			  ->get()
			  ->row_array();	
		return $q;
	
	}
	//get details
	function get_country_details($country_id){
		
	$q = $this
			  ->db
			  ->select('*')
			  ->from('country')
			  ->where('id', $country_id)
			  ->get()
			  ->result_array();	
		return $q;
	
	}
	
	function get_region_details($region_id){
		
	$q = $this
			  ->db
			  ->select('*')
			  ->from('region')
			  ->where('id', $region_id)
			  ->get()
			  ->result_array();	
		return $q;
	
	}
	
	function get_suburb_details($suburb_id){
		
	$q = $this
			  ->db
			  ->select('*')
			  ->from('suburb')
			  ->where('id', $suburb_id)
			  ->get()
			  ->result_array();	
		return $q;
	
	}
	
	function get_location_details($location_id){
		
	$q = $this
			  ->db
			  ->select('*')
			  ->from('location')
			  ->where('id', $location_id)
			  ->get()
			  ->result_array();	
		return $q;
	
	}
	
	//actual update
	
	function update_country($data,$country_id){
				
		$this->db->where('id',$country_id);
		$this->db->update('country',$data);
	
	}
	
	function update_region($data,$region_id){
				
		$this->db->where('id',$region_id);
		$this->db->update('region',$data);
	
	}
	
	function update_suburb_add($customer_id,$suburb_id){
		
		$data = array('surbub_id'=>$suburb_id);		
		$this->db->where('id',$customer_id);
		$this->db->update('customer',$data);
	
	}
	
	function update_location($data,$location_id){
				
		$this->db->where('id',$location_id);
		$this->db->update('location',$data);
	
	}
	
	function enable_country($country_id,$status){
	
			$data = array('enabled'=>$status);
			$this->db->where('id',$country_id);
			$this->db->update('location_country',$data);
	}
	
	function enable_region($region_id,$status){
	
			$data = array('enabled'=>$status);
			$this->db->where('id',$region_id);
			$this->db->update('location_region',$data);
	}
	
	function enable_suburb($suburb_id,$status){
	
			$data = array('enabled'=>$status);
			$this->db->where('id',$suburb_id);
			$this->db->update('location_suburb',$data);
	}
	
	function enable_location($location_id,$status){
	
			$data = array('enabled'=>$status);
			$this->db->where('id',$location_id);
			$this->db->update('location',$data);
	}
	
	//atual insert
	
	function add_country($data){
	
		$insert = $this->db->insert('location_country',$data);
		return $insert;
	}
	
	function add_region($data){
	
		$insert = $this->db->insert('location_region',$data);
		return $insert;
	}
	
	function add_suburb($data){
	
		$insert = $this->db->insert('location_suburb',$data);
		return $insert;
	}
	
	function add_location($data){
	
		$insert = $this->db->insert('location',$data);
		return $insert;
	}
	
	
	function update_suburb_id($user_id,$suburb_id){
		
		
		$data = array(
				'suburb_id' => $suburb_id
		);
		$this->db->where('id', $user_id);
		$this->db->update('customer', $data);
	
	}
}

?>
