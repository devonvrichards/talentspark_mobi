<?php

class Stories_model extends CI_Model {

	  function __construct()
	  {
		parent::__construct();
	  }
	
	function get_stories($per_page, $offset, $category_id)
	{
			$q = $this
			  ->db
			  ->select('*,(select displayname from customer_parent where id = story.customer_id) as author_name,(select name from suburb where id = story.suburb_id) as location')
			  ->from('story')
			  ->where('category_id',$category_id)
			  ->where('enabled', 1)
			  ->limit($per_page, $offset) 
			  ->order_by('timestamp', 'desc')
			  ->get()
			  ->result_array();
		return $q;	
	}
	
	function get_users($per_page, $offset, $category_id)
	{
			$q = $this
			  ->db
			  ->select('*,(select displayname from customer_parent where id = customer_talents.customer_id) as author_name,(select status_message from customer_information where customer_id = customer_talents.customer_id) as status')
			  ->from('customer_talents')
			  ->where('category_id',$category_id)
			  ->limit($per_page, $offset) 
			  ->order_by('timestamp', 'desc')
			  ->get()
			  ->result_array();
		return $q;	
	}
	
	function get_talented_users($per_page, $offset, $category_id)
	{
		$q = $this
			  ->db
			  ->select('*,(select displayname from customer_parent where id = user_talents.customer_id) as name,(select status_message from add_info where customer_id = user_talents.customer_id) as user_status')
			  ->from('user_talents')
			  ->where('category_id',$category_id)
			  ->limit($per_page, $offset) 
			  ->get()
			  ->result_array();
		return $q;	
	}
	function get_story($story_id)
	{
			$q = $this
			  ->db
			  ->select('*, (select name from category where id = story.category_id) as category_name,
					(select displayname from customer_parent where id = story.customer_id) as author_name,
					(select count(id) from comment_story where enabled  = 1 and story_id  = story.id) as comment_num')
			  ->from('story')
			  ->where('enabled', 1)
			  ->where('id', $story_id)
			  ->get()
			  ->row_array();
		return $q;	
	}
	
	function check_followering($customer_id, $story_id){
			
		$q = $this
			  ->db
			  ->select('count(*) as total')
			  ->from('personlised_events')
			  ->where('story_id', $story_id)
			  ->where('customer_id',$customer_id)
			  ->get()
			  ->row_array();
		return $q['total'];
		
	}
	
	function check_talented_user_followering($viewing_customer_id, $customer_id)
	{
			
		$q = $this
			  ->db
			  ->select('count(*) as total')
			  ->from('personlised_customers')
			  ->where('following_customer_id', $viewing_customer_id)
			  ->where('customer_id',$customer_id)
			  ->get()
			  ->row_array();
		return $q['total'];
		
	}
	
	function get_story_count($category_id)
	{
		$q = $this
			  ->db
			  ->select('count(id) as total')
			  ->from('story')
			  ->where('enabled', 1)
			  ->where('category_id',$category_id)
			  ->get()
			  ->row_array();
		return $q['total'];	
	}
	
	function get_user_count($category_id)
	{
		$q = $this
			  ->db
			  ->select('count(id) as total')
			  ->from('customer_talents')
			  ->where('category_id',$category_id)
			  ->get()
			  ->row_array();
		return $q['total'];	
	}
	
	
	function get_customer_name($customer_id)
	{
		$q = $this
			  ->db
			  ->select('name')
			  ->from('customer_parent')
			  ->where('id',$customer_id)
			  ->get()
			  ->row_array();
		return $q;	
	}
	
	function get_category_name($category_id)
	{
		$q = $this
			  ->db
			  ->select('name')
			  ->from('category')
			  ->where('id',$category_id)
			  ->get()
			  ->row_array();
		return $q;	
	}
	
	function get_categories()
	{
			$q = $this
				  ->db
				  ->select('*,(select count(*) from story where category_id = category.id and enabled = 1) as total,(select count(*) from customer_talents where category_id = category.id ) as talent_total')
				  ->from('category')
				  ->where('enabled','1')
				  ->having('total >','0') 
				  ->get()
				  ->result_array();
			return $q;
	}
	function get_author_details($customer_id){
		$q = $this
				  ->db
				  ->select('date_registered, displayname, (select count(*) from story where enabled = 1 and customer_id = customer.id) as no_publisher_articles')
				  ->from('customer')
				  ->where('id',$customer_id)
				  ->get()
				  ->row_array();
			return $q;	
	}
	function list_author_stories($user_id){
	 	$q = $this
				  ->db
				  ->select('id, title, (select count(rating_comment_id) from rating_comment_story_binary where rating_comment_id = 1 and story_id=story.id) as likes')
				  ->from('story')
				  ->where('customer_id', $user_id)
				  ->where('enabled' , 1)
				  ->get()
				  ->result_array();	
			return $q;
 	}	
	
	function check_rating_binary($user_id,$story_id){
		$q = $this
			  ->db
			  ->select('*')
			  ->from('rating_comment_story_binary')
			  ->where('story_id', $story_id)
			  ->where('customer_id', $user_id)
			  ->get();
		if ($q->num_rows === 1)
		{
			return TRUE;
		}
		else
		{
			return FALSE;
		}
		
	}
	public function get_rating_details($story_id, $rating_comment_id){
		$q = $this
			  ->db
			  ->select('count(id) as total')
			  ->from('rating_comment_story_binary')
			  ->where('story_id', $story_id)
			  ->where('rating_comment_id', $rating_comment_id)
			  ->get();
		if ($q->num_rows === 1)
		{
			$num  = $q->row_array();
			return $num['total'];
		}
		else
		{
			return 0;
		}
	}
	function insert_rating_binary($user_id, $story_id, $rating_comment_id){
		$new_rating_comment = array('story_id'=> $story_id, 
						'customer_id'=>$user_id, 
							'rating_comment_id'=>$rating_comment_id);
		$insert = $this->db->insert('rating_comment_story_binary', $new_rating_comment);
		return $insert;
	}
	
	
	
	function insert_message_to_talented($sending_customer_id, $customer_id,$text)
	{
		$data = array('customer_id'=> $customer_id, 
						'customer_id_2'=>$sending_customer_id, 
							'text'=>$text);
		$this->db->insert('customer_inbox', $data);
	}
	
	
	function get_count_pics($story_id)
	{
		 	$q = $this
				  ->db
				  ->select('count(id) as total')
				  ->from('story_media_events')
				  ->where('story_id', $story_id)
				  ->where('status' , 1)
				  ->get()
				  ->result_array();	
			return $q;
	
	}
	
	
	function get_count_talented_pics($customer_id)
	{
		 	$q = $this
				  ->db
				  ->select('count(id) as total')
				  ->from('story_media')
				  ->where('customer_id', $customer_id)
				  ->where('status' , 1)
				  ->get()
				  ->result_array();	
			return $q;
	
	}
	
	function get_talent_details($customer_id)
	{
		 		$q = $this
			  ->db
			  ->select('*, (select background from customer_information where customer_id = customer_parent.id) as background_info,
			  (select suburb_id(select name from suburb where suburb_id = suburb.id) as location from customer_mxit_extra where parent_id ='.$customer_id)
			  ->from('customer_parent')
			  ->where('id',$customer_id)
			  ->get()
			  ->row_array();
		return $q;	
	
	}
	
	function get_photos($customer_id)
	{
			$q = $this
			  ->db
			  ->select('*')
			  ->from('story_media')
			  ->where('customer_id', $customer_id)
			  ->where('status', 1)
			  ->get()
			  ->result_array();
		return $q;
	
	}
}
