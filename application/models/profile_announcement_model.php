<?php

class Profile_announcement_model extends CI_Model {

	function get_all_announcements($user_id,$story_id){
    	$q = $this
  			  ->db
  			  ->select('*')
  			  ->from('customer_event_announcement')
  			  ->where('customer_id', $user_id)
			    ->where('story_id', $story_id)
  			  ->get()
  			  ->result_array();
  		return $q;
    }
	function add_announcement($user_id, $announcement_text,$story_id){
		$new_announcement = array(
				'customer_id'=> $user_id, 
				'story_id'=> $story_id, 
					'message'=> $announcement_text);
	
		$insert = $this->db->insert('customer_event_announcement', $new_announcement);
		return $insert;
	}
	
	function get_announcements_count($user_id,$story_id){
    	$q = $this
  			  ->db
  			  ->select('count(*) as num_of_announcements')
  			  ->from('customer_event_announcement')
  			  ->where('customer_id', $user_id)
			    ->where('story_id', $story_id)
  			  ->get()
  			  ->row_array();
  		return $q['num_of_announcements'];
    }
	
	function get_story_name($story_id)
	{
		$q = $this
  			  ->db
  			  ->select('title')
  			  ->from('story')
			   ->where('id', $story_id)
  			  ->get()
  			  ->row_array();
  		return $q['title'];
	
	}
	
	function get_followers($story_id)
	{
		$q = $this
  			  ->db
  			  ->select('customer_id,(select associative_id from customer where id = personlised_events.customer_id) as associative_id')
  			  ->from('personlised_events')
			   ->where('story_id', $story_id)
  			  ->get()
  			  ->result_array();
  		return $q;
	
	}
	
}