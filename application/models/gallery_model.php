<?php

class Gallery_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function record_insert($customer_id, $file_name)
    {
        $data = array('customer_id' => $customer_id,
            'story_media_type_id'=>1,
            'file_name' => $file_name,
            'status'=>1);
        $this->db->insert('story_media', $data);
    }

    function insert_audio($customer_id, $file_name)
    {
        $data = array('customer_id'=> $customer_id,
            'file_name'=> $file_name,
            'story_media_type_id'=>2,
            'status'=>1);
        //print_r($data);die();
        $this->db->insert('story_media', $data);
    }

    function get_asso_id($customer_id)
    {
        $q = $this
            ->db
            ->select('associative_id')
            ->from('customer')
            ->where('id', $customer_id)
            ->get()
            ->row_array();
        return $q;
    }

    function get_photos($customer_id)
    {
       /* $q = $this
            ->db
            ->select('*')
            ->from('story_media')
            ->where('customer_id', $customer_id)
            ->where('status', 1)
            ->or_where('status', 2)
            ->where('customer_id', $customer_id)
            ->where('story_media_type_id', 1)
            ->get()
            ->result_array();*/
        $where = 'status = 1 AND customer_id = '.$customer_id.' AND story_media_type_id = 1';
        $q = $this
            ->db
            ->select('*')
            ->from('story_media')
            ->where($where)
            //->where('status', 2)
            ->get()
            ->result_array();
        return $q;
    }

    function get_audios($customer_id)
    {
        /*$q = $this
            ->db
            ->select('*')
            ->from('story_media')
            ->where('customer_id', $customer_id)
            ->where('status', 1)
            ->or_where('status', 2)
            ->where('customer_id', $customer_id)
            ->where('story_media_type_id', 2)
            ->get()
            ->result_array();*/
        $where = 'status = 1 AND customer_id = '.$customer_id.' AND story_media_type_id = 2';
        $q = $this
            ->db
            ->select('*')
            ->from('story_media')
            ->where($where)
            ->get()
            ->result_array();
        return $q;
    }
	
	

    function update_status($story_media_id)
    {
        $data = array(
            'status' => 0
        );

        $this->db->where('id', $story_media_id);
        $this->db->update('story_media', $data);
    }


}

