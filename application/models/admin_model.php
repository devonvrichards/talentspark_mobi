<?php
class Admin_model extends CI_Model {

    function __construct()
    {
        parent::__construct();
    }

	function get_details($id)
	{
		$q = $this
			  ->db
			  ->select('id')
			  ->from('customer_mobi_extra')
			  ->where('id', $id)
			  ->get()
			  ->row_array();
		return $q['id'];
	}
	
	function get_details_after($id)
	{
		$q = $this
				  ->db
				  ->select('*')
				  ->from('customer_mobi_extra')
				  ->where('id',$id)
				  ->get()
				  ->row_array();
			return $q;
	}
	
	
	
}
