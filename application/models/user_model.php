<?php

class User_model extends CI_Model
{

    function __construct()
    {
        parent::__construct();
    }

    function is_user($user_id)
    {
        $q = $this
            ->db
            ->select('id')
            ->from('customer_mobi_extra')
            ->where('id', $user_id)
            ->get();

        if ($q->num_rows === 1) {
            return $q->row_array();
        } else {
            return FALSE;
        }
    }

    function create($username,$password, $area, $dob, $gender)
    {
        $data = array(
            'associative_id' => $mxit_id,
            'area' => $area,
            'dob' => $dob,
            'gender' => $gender
        );

        $this->db->insert('customer', $data);

        return $this->db->insert_id();
    }

    function get_user_type($user_id)
    {
        $q = $this
            ->db
            ->select('customer_type_id')
            ->from('customer_parent')
            ->where('id', $user_id)
            ->get()
            ->row_array();
        return $q;
    }

    function get_user_parent_id($mxit_id)
    {
        $q = $this
            ->db
            ->select('customer_parent_id')
            ->from('customer_mxit_extra')
            ->where('associative_id', $mxit_id)
            ->get()
            ->row_array();
        return $q;
    }
}

/* End of file user_model.php /
/ Location: ./application/models/user_model.php */
