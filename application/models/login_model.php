<?php

class Login_model extends CI_Model {

  function __construct()
  {
  parent::__construct();
  }

	 function validate_user($username,$password)
	 {
		 $encrypted = md5($password);
		 $q = $this
		 ->db
		 ->select('*')
		 ->from('customer_mobi_extra')
		 ->where('username', $username)
		  ->where('password', $encrypted)
		 ->get()
		->result_array();
		return $q;
	 }
	 
	 function validate_username($username)
	  {
		 $q = $this
		 ->db
		 ->select('*')
		 ->from('customer_mobi_extra')
		 ->where('username', $username)
		 ->get()
		->result_array();
		return $q;
	 }

    function create_parent_user()
    {
        $data = array(
           'customer_type_id' => 1
        );

        $this->db->insert('customer_parent', $data);

    }

	 function create_mobi_user($username,$password,$parent_id)
	 {
		  $encrypted = md5($password);

		  $data = array(
         'customer_parent_id'  =>$parent_id,
		 'username'	=> $username,
		 'password' => $encrypted

		);

		$this->db->insert('customer_mobi_extra', $data);

	 }

}

/* End of file user_model.php /
/ Location: ./application/models/user_model.php */
